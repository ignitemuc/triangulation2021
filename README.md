# Triangulating difficult nodes in the animal tree of life #

We analysed 47 genomes based proteomes with know tools and personalized codes. 

This repository includes all the codes used to analyse the data for the various phylogenies.

The results obtained in the different steps can also be found in this repo. 

All listed and described below.


### General workflow: ###

1. Extract proteins from genome based predictions.
2. Check for simillarity all vs. all.
3. Cluster the proteins into groups (homologous and orthologous).
4. Extract ortho- and homogroups.
5. Convert into matrices of absence and presence.
6. Generate phylogeny.


### Programs used: ###

Make sure to install the following tools:

OrthoFinder, MCL, DIAMOND, [homomcl](https://github.com/willpett/homomcl).
 

### Codes in this repository: ###

OrthoFinder_for_MCL_after_DIAMOND.sh - Used to re -run OrthoFinder with different I- and E-values.

Nexus2Fasta.pl - Before pruning convert nexus format matrix back to fasta format data type.

Nexus_Pruning.sh - Used to prune out the species from the matrix.

does_specie_have_gene.py - Used to convert MCL output into fasta format data.

stat_from_bpcomp_tracecomp.sh - Used to check convergence for the 4 chains for all possible combinations to find the best parameters. 

short_names_convert.sh - Used to translate from 4 letters short names of species to the formal names.

Codes for [revBays](https://github.com/willpett/metazoa-gene-content)


### How to re-run the analyses in this study: ###

Create a folder with the full set of proteomes for the species of interest.

**Orthogroups**

Run [OrthoFinder](https://bitbucket.org/ignitemuc/scripts/src/master/OrthoFinder_def_ortho_LBA.sh) with DIAMOND using different E-values.

Alternatively, run OrthoFinder once and just re-run DIAMOND on previously generated results for different E-values.

Use the pre calculated results for MCL step using different I values.

To automate this step use the [code for multiple OrthoFinder](https://bitbucket.org/ignitemuc/scripts/src/master/OrthoFinder_for_MCL_after_DIAMOND.sh) runs.

After generating the orthogroups file (Orthogroups.tsv) format it to fit the revBayes formating in NEXUS using:

1. tsv --> mcl output format
2. [mcl --> matrix of absence and presence](https://bitbucket.org/ignitemuc/scripts/src/master/does_specie_have_gene.py)
3. matrix --> fasta format
4. fasta --> NEXUS

Use the last output as input to the [revBayse analyses](https://github.com/willpett/metazoa-gene-content).

Extract [convergence statistics](https://bitbucket.org/ignitemuc/scripts/src/master/Stats_from_bpcomp_tracecomp.sh) for the phylogenies.

To convert short format names of Species to full scientific name use [Names_convert.sh](https://bitbucket.org/ignitemuc/scripts/src/master/Names_convert.sh).

**Homogroups**

For the homologues prediction after DIAMOND step, [extract length](https://bitbucket.org/ignitemuc/scripts/src/master/Get_seq_length.sh) of the proteins for each species and run [homomcl](https://github.com/willpett/homomcl) to create abc format file. 

If you wish just to have the final matrix use MCL.

If you want to analyse the different behaviors of the clusters use [clmprotocols](https://micans.org/mcl/man/clmprotocols.html).


**LBA**

Tested in this study using default parameters of all tools used. 

Repeat Homogroups and orthogroups prediction as described above but without the long-branched species

Caenorhabditis elegans, Pristionchus pacificus, and Schistosoma mansoni.


**Pruned**

For datasets creation using the pruning method, use initial Opi dataset and the script "Nexus_Pruning.sh".


**Outgroup sampling**


Tested in this study using default parameters of all tools used. 

Repeat Homogroups and orthogroups prediction as described above but reduce the outgroups species

i) the complete taxon sampling; 

ii) Ichthyosporea + Choanoflagellates + Metazoa (= Holozoa; dataset prefix Holo), and

iii) Choanoflagellates + Metazoa (= Choanozoa; dataset prefix Cho) 



### The output of this analysis ###

All *.neuxs files and Supplementary Tables in file X.


### Glossary in this study: ###

_Homogroups_ - A set including homologous proteins that are predicted to be inherited from a common ancestor, all the proteins or parts, can include partial genes, orthologs, xenologs and paralogs. Contain any subset of the species, but no single species homogroups (proteins need to be shared by at least two species).

_Orthogroups_ - A set of orthologous proteins that are predicted to be inherited from a common ancestor and separated by a speciation event, can also include in-paralogs and partial genes. Contain any subset of the species, but no single species homogroups (proteins need to be shared by at least two species).

_Nchar_ - The number of positions in the final alignment (a matrix of alignment from 0 [absence] and 1 [presence]). Each character (column, if matrix is species vs. chars) represents an orthogroup or homogroup (as defined previously) compared along all the species. 

_Total Posterior Consensus Tree (TPCTree)_ - a majority rule consensus tree of all posterior trees from all converged chains of different Bayesian analyses. Two different TPCTrees were estimated: 1) based on equal number of taxa independent from methodology of parameters (TPCTree-all) and 2) based on homogroups or orthogroups methodology (TPCTree-partial). 

_Granulation_ - defined by the inflation parameter (I) in the MCL algorithm. Affects the cluster size, i.e., defines the number of the predicted clusters for homogroups and orthogroups. The size of this parameter creates a scale, where small I values indicate fine-grained clustering, and large values a very coarse grained clustering 37. Increasing the I value leads to further splitting of the largest clusters, therefore more smaller clusters.

_Singleton_ - gene family which is coded as present in only a single species 38.

Naming convention in this work - The initial dataset was Opisthokonta (Opi) and contained data from genomes of 47 species. It was divided into two different subsets: a dataset with only Acoelomorpha (Aco, 44 species) and a dataset with Xenoturbella bocki alone (Xen, 41 species). Also, the three additional datasets were created where Fungi were excluded as outgroups (Holozoa, Holo) and Choanozoa (Cho), where only species of the Choanoflagellata were included as outgroup. Further subsets excluded certain long-branched taxa in the ingroup. The methodology type used for taxon reduction is indicated in the naming convention by “Ab'', for ab initio and “P” for pruning (see Supp. Table 5). For more details see Supp. Figure 1.

### The data structure in the deposited folders: ###

347 directories (2.6 T, commprassed 645GB)

Each one of the main folders (0,1,2) has its README with folder structure explanation.

All data can be provided upon request.

### Who do I talk to? ###

* Repo owner or admin
